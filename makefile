dev_server:
	make dev_banco
	mvn spring-boot:run

dev_banco:
	docker-compose -f docker/docker-compose.yml up -d

instalar_dependencias:
	mvn clean install

tests:
	mvn clean test

psql_local:
	psql -U root -W --host=localhost -p 5432 --dbname=contactlist
