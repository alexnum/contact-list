package bravi.contactList.repositorys;

import bravi.contactList.models.Contact;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ContactRepository extends JpaRepository<Contact, Long> {
    Optional<Contact> findByIdAndPerson_Id(Long id, Long id_person);
    List<Contact> findByPerson_id(Long id);
}
