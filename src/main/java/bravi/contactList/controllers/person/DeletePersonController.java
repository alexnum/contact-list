package bravi.contactList.controllers.person;

import java.util.Optional;

import com.fasterxml.jackson.core.JsonProcessingException;

import bravi.contactList.controllers.AbstractResponse;
import bravi.contactList.models.Person;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class DeletePersonController extends AbstractResponse {

    @DeleteMapping("/person/{id}")
    public ResponseEntity delete(@PathVariable Long id) throws JsonProcessingException {
        Optional<Person> person = this.personRepository.findById(id);
        String msg = new String("Deletado com sucesso");
        if (!person.isPresent()) {
            this.status = HttpStatus.BAD_REQUEST;
            msg = this.msgErro();
        }
        person.ifPresent(
            personConsumer -> this.personRepository.delete(personConsumer));
        return new ResponseEntity<>(msg, this.status);
    }

    public String msgErro() {
        return "Person isn't registered";
    }

}
