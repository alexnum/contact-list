package bravi.contactList.controllers.contacts;

import java.util.Optional;

import javax.validation.Valid;

import com.fasterxml.jackson.core.JsonProcessingException;

import bravi.contactList.controllers.AbstractResponse;
import bravi.contactList.models.Contact;
import bravi.contactList.repositorys.ContactRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class UpdateContactPersonController extends AbstractResponse {

    @Autowired
    ContactRepository contactRepository;

    @PutMapping("/person/{id_person}/contact/{id_contact}/")
    public ResponseEntity add(
        @PathVariable Long id_person,
        @PathVariable Long id_contact,
        @Valid @RequestBody Contact contactData
    ) throws JsonProcessingException {
        this.status = HttpStatus.OK;
        Optional<Contact> contato = this.contactRepository.findByIdAndPerson_Id(
            id_contact, id_person);
        if (contato.isPresent()) {
            contactData.setId(contato.get().getId());
            contactData.setPerson(this.personRepository.getOne(id_person));
            this.contactRepository.save(contactData);
        } else {
            this.status = HttpStatus.BAD_REQUEST;
        }
        return new ResponseEntity<>(this.json(contactData), this.status);
    }

    public String msgErro() {
        return "Contact isn't registerd to person";
    }

}
