package bravi.contactList.controllers.contacts;

import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;

import bravi.contactList.controllers.AbstractResponse;
import bravi.contactList.models.Contact;
import bravi.contactList.repositorys.ContactRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class ListContactByPersonController extends AbstractResponse {

    @Autowired
    ContactRepository contactRepository;

    @GetMapping("/person/{id}/contact/all")
    public ResponseEntity listAll(@PathVariable Long id) throws JsonProcessingException {
        if (!this.personRepository.existsById(id))
            this.status = HttpStatus.BAD_REQUEST;
        List<Contact> contatos = this.contactRepository.findByPerson_id(id);
        return new ResponseEntity<>(this.json(contatos), this.status);
    }

    public String msgErro() {
        return "Person is not registered";
    }

}
