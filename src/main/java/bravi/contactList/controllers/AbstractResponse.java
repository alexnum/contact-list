package bravi.contactList.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RestController;

import bravi.contactList.repositorys.PersonRepository;

@RestController
public abstract class AbstractResponse {

    @Autowired
    public PersonRepository personRepository;

    public HttpStatus status = HttpStatus.OK;

    public abstract String msgErro();

    public String json(Object objeto) throws JsonProcessingException {
        if (this.status != HttpStatus.OK)
            return this.msgErro();
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(objeto);
    }
}
