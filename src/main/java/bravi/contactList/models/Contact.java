package bravi.contactList.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Size;

import org.apache.commons.validator.EmailValidator; 

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;


@Entity
public class Contact extends ModelBase {

    public enum TipoContato {EMAIL, TELEFONE, WHATSAPP}

    @Column
    String valor;

    @Column
    @Enumerated
    @NotNull(message = "A Contact type must be specified")
    TipoContato tipo;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(nullable=false, name="person_id")
    @OnDelete(action=OnDeleteAction.CASCADE)
    Person person;

    public Contact() {}

    public Contact(String valor, TipoContato tipo) {
        this.valor = valor;
        this.tipo = tipo;
    }

    public Contact(String valor, TipoContato tipo, Person person) {
        this.valor = valor;
        this.tipo = tipo;
        this.person = person;
    }

    public String getValor() {
        return this.valor;
    }

    public TipoContato getTipo() {
        return this.tipo;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public void setTipo(TipoContato tipo) {
        this.tipo = tipo;
    }

    public Long getPerson() {
        return this.person.getId();
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    @AssertTrue
    private boolean isOk() {
        boolean isValid = true;
        if(this.tipo == TipoContato.EMAIL){
            EmailValidator validator = EmailValidator.getInstance();
            isValid = validator.isValid(this.valor);
        }
        return isValid;
    }

}
