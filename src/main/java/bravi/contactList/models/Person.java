package bravi.contactList.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.Size;

@Entity
public class Person extends ModelBase {

    @Column(nullable=false, unique=true)

    @Size(min=3, max=30) 
    String nome;

    public Person() {}

    public Person(String nome) {
        this.nome = nome;
    }

    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

}
