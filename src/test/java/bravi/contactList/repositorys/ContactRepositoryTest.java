package bravi.contactList.repositorys;

import bravi.contactList.models.Contact;
import bravi.contactList.models.Person;
import bravi.contactList.models.Contact.TipoContato;
import bravi.contactList.repositorys.ContactRepository;

import org.junit.runner.RunWith;

import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;


@DataJpaTest
@RunWith(SpringRunner.class)

public class ContactRepositoryTest {

    @Autowired
    ContactRepository contactRepository;

    @Autowired
    PersonRepository personRepository;

    private Person person;
    private Contact contact;

    @Before
    public void setUp() {
        this.person = this.personRepository.save(new Person("Leonardo Ramos Duarte"));
        this.contact = this.contactRepository.save(
            new Contact("lramosduarte@gmal.com", TipoContato.EMAIL, person));
        this.contactRepository.save(
            new Contact("3895191981", TipoContato.TELEFONE, person));
    }

    @Test
    public void findContactByIdContactAndPerson() {
        Optional<Contact> contato = this.contactRepository.findByIdAndPerson_Id(
            this.contact.getId(), this.person.getId());
        Assertions.assertTrue(contato.isPresent());
    }

    @Test
    public void findContactByIdContactAndPersonNoExists() {
        Optional<Contact> contato = this.contactRepository.findByIdAndPerson_Id(
            this.contact.getId(), 10L);
        Assertions.assertFalse(contato.isPresent());
    }

    @Test
    public void findAllContactsByPerson() {
        List<Contact> contatos = this.contactRepository.findByPerson_id(this.person.getId());
        Assertions.assertEquals(2, contatos.size());
    }

}
