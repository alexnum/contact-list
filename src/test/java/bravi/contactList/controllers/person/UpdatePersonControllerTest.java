package bravi.contactList.controllers.person;

import bravi.contactList.controllers.BaseTestController;
import bravi.contactList.models.Person;
import bravi.contactList.repositorys.PersonRepository;
import net.minidev.json.JSONObject;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.HashMap;


public class UpdatePersonControllerTest extends BaseTestController {

    @Autowired
    PersonRepository personRepository;

    @Test
    public void updatePersonByIdWithSuccess() throws Exception {
        String nomeEsperado = new String("Thrall son of Durotan");
        Person person = this.personRepository.save(new Person("Leonardo Ramos Duarte"));
        HashMap<String, String> dados = new HashMap<String, String>() {{
            put("nome", nomeEsperado);
        }};
        this.mockMvc.perform(
            put(String.format("/person/%d", person.getId()))
            .contentType(MediaType.APPLICATION_JSON)
            .content(new org.json.JSONObject(dados).toString())
        );
        person = this.personRepository.findById(person.getId()).get();
        Assertions.assertEquals(nomeEsperado, person.getNome());
    }

    @Test
    public void updatePersonByIdWithSuccessReceiveOk() throws Exception {
        Person person = this.personRepository.save(new Person("Leonardo Ramos Duarte"));
        HashMap<String, String> dados = new HashMap<String, String>() {{
            put("nome", "Thrall son of Durotan");
        }};
        this.mockMvc.perform(
            put(String.format("/person/%d", person.getId()))
            .contentType(MediaType.APPLICATION_JSON)
            .content(new org.json.JSONObject(dados).toString())
        ).andExpect(status().isOk());
    }

    @Test
    public void updatePersonByIdInvalidReceiveErro() throws Exception {
        this.mockMvc.perform(
            put(String.format("/person/%d", 1))
            .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().isBadRequest());
    }

}
