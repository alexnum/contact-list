package bravi.contactList.controllers.person;

import bravi.contactList.controllers.BaseTestController;
import bravi.contactList.models.Person;
import bravi.contactList.repositorys.PersonRepository;

import org.junit.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


public class GetPersonControllerTest extends BaseTestController {

    @Autowired
    PersonRepository personRepository;

    @Test
    public void getPersonByIdWithSuccess() throws Exception {
        Person person = this.personRepository.save(new Person("Leonardo Ramos Duarte"));
        this.mockMvc.perform(
            get(String.format("/person/%d", person.getId()))
            .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().isOk());
    }

    @Test
    public void getPersonInvalidByIdReceiveError() throws Exception {
        this.mockMvc.perform(
            get(String.format("/person/%d", 1))
            .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().isBadRequest());
    }

}
