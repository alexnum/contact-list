package bravi.contactList.controllers.person;

import bravi.contactList.controllers.BaseTestController;
import bravi.contactList.models.Person;
import bravi.contactList.repositorys.PersonRepository;
import net.minidev.json.JSONObject;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.HashMap;


public class ListPersonControllerTest extends BaseTestController {

    @Autowired
    PersonRepository personRepository;

    @Test
    public void listaPessoasCadastradasRecebeOk() throws Exception {
        this.personRepository.save(new Person("Leonardo Ramos Duarte"));
        this.personRepository.save(new Person("Thrall son of Durotan"));
        this.personRepository.save(new Person("Riki de lothar"));
        this.mockMvc.perform(
            get("/person/all")
            .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().isOk());
    }

    @Test
    public void ListaPessoasVaziaRecebeOk() throws Exception {
        this.mockMvc.perform(
            get("/person/all")
            .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().isOk());
    }

}
