package bravi.contactList.controllers.contacts;

import bravi.contactList.controllers.BaseTestController;
import bravi.contactList.models.Person;
import bravi.contactList.repositorys.PersonRepository;
import net.minidev.json.JSONObject;

import org.junit.Test;
import org.junit.Before;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


public class AddContactPersonControllerTest extends BaseTestController {

    @Autowired
    PersonRepository personRepository;

    private Person person;

    @Before
    public void setUp() {
        this.person = this.personRepository.save(new Person("Leonardo Ramos Duarte"));
    }

    @Test
    public void CadastraNovoContatoParaPessoaComSucesso() throws Exception {
        JSONObject json = new JSONObject();
        json.put("valor", "lramosduarte@gmail.com");
        json.put("tipo", 0);
        this.mockMvc.perform(
            post(String.format("/person/%d/contact", this.person.getId()))
            .contentType(MediaType.APPLICATION_JSON)
            .content(json.toString())
        ).andExpect(status().isOk());
    }

    @Test
    public void tentaCadastrarNovoContatoEmailInvalidoRecebeErro() throws Exception {
        JSONObject json = new JSONObject();
        json.put("valor", "lramosduarte@@");
        json.put("tipo", 0);
        this.mockMvc.perform(
            post(String.format("/person/%d/contact", this.person.getId()))
            .contentType(MediaType.APPLICATION_JSON)
            .content(json.toString())
        ).andExpect(status().isBadRequest());
    }

    @Test
    public void tentaCadastrarNovoContatoSemTipoRecebeErro() throws Exception {
        JSONObject json = new JSONObject();
        json.put("valor", "lramosduarte@gmail.com");
        this.mockMvc.perform(
            post(String.format("/person/%d/contact", this.person.getId()))
            .contentType(MediaType.APPLICATION_JSON)
            .content(json.toString())
        ).andExpect(status().isBadRequest());
    }

    @Test
    public void tentaCadastrarNovoContatoUsuarioInexistenteRecebeErro() throws Exception {
        JSONObject json = new JSONObject();
        json.put("valor", "lramosduarte@gmail.com");
        json.put("tipo", 0);
        this.mockMvc.perform(
            post(String.format("/person/%d/contact", 10))
            .contentType(MediaType.APPLICATION_JSON)
            .content(json.toString())
        ).andExpect(status().isBadRequest());
    }

}
