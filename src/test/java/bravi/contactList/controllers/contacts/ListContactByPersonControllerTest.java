package bravi.contactList.controllers.contacts;

import bravi.contactList.controllers.BaseTestController;
import bravi.contactList.models.Contact;
import bravi.contactList.models.Person;
import bravi.contactList.models.Contact.TipoContato;
import bravi.contactList.repositorys.ContactRepository;
import bravi.contactList.repositorys.PersonRepository;

import org.junit.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


public class ListContactByPersonControllerTest extends BaseTestController {

    @Autowired
    PersonRepository personRepository;

    @Autowired
    ContactRepository contactRepository;

    @Test
    public void listaTodosContatosDaPessoaInformadaComSucessoRecebeOk() throws Exception {
        Person person = this.personRepository.save(new Person("Leonardo Ramos Duarte"));
        this.contactRepository.save(new Contact("123123", TipoContato.TELEFONE, person));
        this.mockMvc.perform(
            get(String.format("/person/%d/contact/all", person.getId()))
            .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().isOk());
    }

    @Test
    public void tentaListarContatosPessoaInvalidaRecebeErro() throws Exception {
        this.mockMvc.perform(
            get(String.format("/person/%d/contact/all", 10))
            .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().isBadRequest());
    }

}
