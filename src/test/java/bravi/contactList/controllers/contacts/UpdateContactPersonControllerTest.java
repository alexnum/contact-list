package bravi.contactList.controllers.contacts;

import bravi.contactList.controllers.BaseTestController;
import bravi.contactList.models.Contact;
import bravi.contactList.models.Person;
import bravi.contactList.models.Contact.TipoContato;
import bravi.contactList.repositorys.ContactRepository;
import bravi.contactList.repositorys.PersonRepository;
import net.minidev.json.JSONObject;

import org.junit.Test;
import org.junit.Before;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


public class UpdateContactPersonControllerTest extends BaseTestController {

    @Autowired
    PersonRepository personRepository;

    @Autowired
    ContactRepository contactRepository;

    private Person person;
    private Contact contato;

    @Before
    public void setUp() {
        this.person = this.personRepository.save(new Person("Leonardo Ramos Duarte"));
        this.contato = this.contactRepository.save(
            new Contact("123123", TipoContato.TELEFONE, person));
    }

    @Test
    public void atualizaDadosContatoDeUmaPessoComSucessoRecebeOk() throws Exception {

        JSONObject json = new JSONObject();
        json.put("valor", "mail@mail.com");
        json.put("tipo", 0);
        this.mockMvc.perform(
            put(String.format("/person/%d/contact/%d/", this.person.getId(), this.contato.getId()))
            .contentType(MediaType.APPLICATION_JSON)
            .content(json.toString())
        ).andExpect(status().isOk());
    }

    @Test
    public void tentaAtualizarContatoComEmailInvalidoRecebeErro() throws Exception {
        JSONObject json = new JSONObject();
        json.put("valor", "invalidmail@@");
        json.put("tipo", 0);
        this.mockMvc.perform(
            put(String.format("/person/%d/contact/%d/", this.person.getId(), this.contato.getId()))
            .contentType(MediaType.APPLICATION_JSON)
            .content(json.toString())
        ).andExpect(status().isBadRequest());
    }

    @Test
    public void tentaAtualizarContatoInvalidoRecebeErro() throws Exception {
        this.mockMvc.perform(
            put(String.format("/person/%d/contact/%d/", 10, 12))
            .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().isBadRequest());
    }

}
