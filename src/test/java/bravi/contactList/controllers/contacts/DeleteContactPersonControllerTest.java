package bravi.contactList.controllers.contacts;

import bravi.contactList.controllers.BaseTestController;
import bravi.contactList.repositorys.ContactRepository;
import bravi.contactList.repositorys.PersonRepository;

import org.junit.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


public class DeleteContactPersonControllerTest extends BaseTestController {

    @Autowired
    PersonRepository personRepository;

    @Autowired
    ContactRepository contactRepository;


    @Test
    public void tentaDeletarContatoQueNaoExisteRecebeErro() throws Exception {
        this.mockMvc.perform(
            delete("/person/%d/contact/%d", 2, 2)
            .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().isBadRequest());
    }

}
