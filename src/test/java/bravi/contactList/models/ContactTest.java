package bravi.contactList.models;

import bravi.contactList.models.Contact;
import bravi.contactList.models.Person;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;;


@RunWith(SpringRunner.class)
@SpringBootTest
public class ContactTest {

    @Test
    public void criaModelComSucesso() {
        Person pessoa = new Person("Leonardo Ramos Duarte");
        new Contact("lramosduarte@gmail.com", Contact.TipoContato.EMAIL, pessoa);
    }

}
